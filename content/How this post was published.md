+++
title = "How this post was published"
description = "A static blog using Gutenberg and GitLab CI"

# The date of the post.
# 2 formats are allowed: YYYY-MM-DD (2012-10-02) and RFC3339 (2002-10-02T15:00:00Z)
# Do not wrap dates in quotes, the line below only indicates that there is no default date.
# If the section variable `sort_by` is set to `date`, then any page that lacks a `date`
# will not be rendered.
date = 2018-09-09

# The weight as defined in the Section page
# If the section variable `sort_by` is set to `weight`, then any page that lacks a `weight`
# will not be rendered.
# weight = 0

# A draft page will not be present in prev/next pagination
draft = false

# If filled, it will use that slug instead of the filename to make up the URL
# It will still use the section path though
# slug = ""

# The path the content will appear at
# If set, it cannot be an empty string and will override both `slug` and the filename.
# The sections' path won't be used.
# It should not start with a `/` and the slash will be removed if it does
# path = ""

# Use aliases if you are moving content but want to redirect previous URLs to the
# current one. This takes an array of path, not URLs.
aliases = []

# Whether the page should be in the search index. This is only used if
# `build_search_index` is set to true in the config and the parent section
# hasn't set `in_search_index` to false in its front-matter
in_search_index = true

# Template to use to render this page
# template = "page.html"

# The taxonomies for that page. The keys need to be the same as the taxonomies
# name configured in `config.toml` and the values an array of String like
[taxonomies]
tags = ["Rust", "GitLab", "CI/CD", "Gutenberg"]

# Your own data
[extra]
+++

As the first post of my new blog, I thought it'd be nice to explain a little about how this post was published. In order to make publishing new posts as painless as possible, I've completely automated building and deployment of new versions of this blog. This post will show you how I've set this up.

<!-- more -->
## Gutenberg

Given my interest in [Rust](https://www.rustlang.org) and the numerous hobby projects I've used it for in the past couple of years, I've picked a Rust application for my blog as well: the static site generator [Gutenberg](https://www.getgutenberg.io/). Gutenberg has the features I need (at this time, anyway) and the use of Rust will make it more enjoyable to contribute to as well.

## Continuous Deployment

The CI/CD pipeline is a fairly typical Continuous Deployment with manual approval workflow. As with my other personal projects lately, I'll be using GitLab for this blog. I quite like some of its features and particularly their tight integration.

The first thing I do when I start working on a change to this blog, is create a GitLab Issue, see [this example](https://gitlab.com/mtsr/blog/issues/3). From this I can create a [merge request and a git branch]((https://gitlab.com/mtsr/blog/merge_requests/10)) with the touch of a button. As soon as the branch is created, a new instance of the [CI pipeline](https://gitlab.com/mtsr/blog/pipelines/29761567) is started, deploying a copy of the blog to a new review environment at [https://www.jonasmatser.nl/blog-review/3-example-issue/](https://www.jonasmatser.nl/blog-review/3-example-issue/).

[pipeline]: /pipeline.png
![GitLab CI Pipeline][pipeline]

After that, any commits to the branch that I push to GitLab trigger the pipeline again, to automatically build and deploy an updated review environment. You can see the most recent pipeline runs [here](https://gitlab.com/mtsr/blog/pipelines).

After I'm happy with the changes, I accept the merge request using the GitLab UI, triggering the pipeline for `master`. This deploys the new version to a `staging` environment. After reviewing the staging deployment from the [Environments Dashboard](https://gitlab.com/mtsr/blog/environments), I manually approve the version for deployment to `production`. This last step isn't really necessary for a simple blog, but I like it as a demonstation for more conservative organisations, such as my current employer.

## The pipeline

GitLab CI is configured using `.gitlab-ci.yaml` in your source repository to define the pipeline for a project. I'll show the highlights of [the one for this blog](https://gitlab.com/mtsr/blog/blob/master/.gitlab-ci.yml). The complete documentation for the configuration of GitLab CI can be found [here](https://docs.gitlab.com/ce/ci/yaml/README.html).

At the start of the file one generally finds some defaults, such as `variables` or `image` used for later build jobs, but this blog didn't need any at this time. (Note that since the items are named, the order isn't significant and other people may use a different one.)

Next one finds the `stages`, which defines the consecutive stages executed by the pipeline.

```yaml
stages:
  - build
  - deploy-review
  - deploy-staging
  - deploy-production
```

### Build
Excluding a number of keywords (such as `stages`), every unindented line begins a new build job definition, starting with its name.

```yaml
build-review:
  image: registry.gitlab.com/mtsr/gutenberg-docker:latest
  stage: build
  variables:
    URL: https://jonasmatser.nl/blog-review/$CI_COMMIT_REF_SLUG
  script:
    - gutenberg build -u $URL
  artifacts:
    paths:
      - /mtsr/blog/public
    expire_in: 1 week
  only:
    - branches
  except:
    - master
```

Each build job is executed using a docker `image`. If undefined, this will default to an image set by the CI runner. In this case we use the gutenberg Docker image I created [here](https://gitlab.com/mtsr/gutenberg-docker).

Next we declare the `stage` this job is part of, e.g. `build`. Note that jobs within a stage are run in parallel.

Then we declare a variable `URL`, which is made available to scripts in this job. `$CI_COMMIT_REF_SLUG` is provided by GitLab CI and contains a url-safe slug of the current branch name.

`script` is an array of shell commands that will be executed in order as long as each exits with code 0. In our case we call Gutenberg to build the blog with the base-url for the review environment.

`artifacts` is a way to pass files from one build job to another and additionally makes the files available for download from the pipeline page. I've set this download to be automatically removed after 1 week, which incidentally sets a 1 week window on manual approval of new versions from staging to production.

`only` limits this job to being run on branches, `except` the `master` branch.

### Deploy

Below is the deploy job for the review environment. For this job we use another custom docker image. <sub>Don't mind the use of FTP, I know it's ugly, blame my current hosting provider.</sub>

```yaml
deploy-review:
  image: registry.gitlab.com/mtsr/lftp-docker:latest
  stage: deploy-review
  variables:
    GIT_STRATEGY: none
  script:
    - lftp -e "mkdir $CI_COMMIT_REF_SLUG; mirror -R -e public $CI_COMMIT_REF_SLUG --verbose" -u ${REVIEW_FTP_USERNAME},${REVIEW_FTP_PASSWORD} ${FTP_HOSTNAME}
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://jonasmatser.nl/blog-review/$CI_COMMIT_REF_SLUG
    on_stop: stop-review
  dependencies:
    - build-review
  only:
    - branches
  except:
    - master
```

We use `GIT_STRATEGY: none` to skip cloning the git repository for this job, since we already have everything we need as an artifact from the build job.

Using `environment` we define an environment for the GitLab [Environments Dashboard](https://gitlab.com/mtsr/blog/environments), allowing us to see the version of this deployment, a link to the deployment and more from the GitLab interface.

[environment_dashboard]: /environment_dashboard.png
![Alt text][environment_dashboard]

A useful detail is the `on_stop` property, which defines the build job to call when the environment can be cleaned up (for example when the branch is removed, or when the environment is manually removed).

The final new setting is `dependencies`, which tells GitLab CI we want to download the artifact from the `build-review` job for this build job.

### On Stop

The last new piece of our CI definition defines how the review environment is actually stopped.

```yaml
stop-review:
  image: registry.gitlab.com/mtsr/lftp-docker:latest
  stage: deploy-review
  variables:
    GIT_STRATEGY: none
  script:
    - lftp -e "rm -r -f $CI_COMMIT_REF_SLUG" -u ${REVIEW_FTP_USERNAME},${REVIEW_FTP_PASSWORD} ${FTP_HOSTNAME}
  only:
    - branches
  except:
    - master
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
```

The other jobs of our CI file deploy to staging and production, neither of which needs to be stopped, so `stop-review` is the only `on_stop` job we use.

## Automation

Although automation isn't a goal on its own, a setup like the above really helps to streamline processes.

For example, I can edit a piece of the blog on my phone while on the go and be sure of the quality of the result. The automated deployment even allows me to do a quick manual check, before hitting the merge button.

And when processes get that much more complicated, as is often the case with software development, the payoff is even bigger.