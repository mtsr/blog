# Blog

## Development

```zola serve```

## Build

```zola build```

Results in `public` directory.

## Theme

Uses a [fork of the Even theme](https://github.com/mtsr/even) with minor styling changes.
